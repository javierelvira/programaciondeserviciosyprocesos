#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

sig_atomic_t sigusrl_count = 0; //creamos la cuenta de veces que se manda la señal
int child_status; //estado del hijo
pid_t child_pid; //numero de pid del hijo

void handler (int signal_number){ //funcion para ir mandando las señales
    ++sigusrl_count;
}

void spawn(){

    child_pid = fork(); //crea el fork para hacer los dos procesos
    if(child_pid != 0) { //si el pid del hijo es distinto de 0 se llama a handler

        //padre
        handler(1);
        handler(2);
        handler(3);
    }
    else{
        //hijo

//dos procesos
//while no sea 3, el hijo duerme con usleep
//cuando el padre mande la señal kill, se manda la señal de sigusr1 y se llama al handler

        if(sigusrl_count >= 3){ //si la cuenta de veces que se manda la señal es = a 3 se mata al hijo
            printf("El hijo muere");
            kill(getpid(), SIGTERM);
        }
    }
}

int main() {

    struct sigaction sa; //creacion de estructura para mandar las señales
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = &handler;
    sigaction (SIGUSR1, &sa, NULL);

    spawn();
    wait(&child_status);

    printf("señales: %i\n", sigusrl_count);

    return EXIT_SUCCESS;
}
