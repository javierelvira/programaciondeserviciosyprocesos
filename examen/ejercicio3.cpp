#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#define TLADRILLOS 600
#define TIEMPOCREACION 10000

int almacen = 0;
pthread_mutex_t llaveAlmacen = PTHREAD_MUTEX_INITIALIZER;
bool estadoFabrica = false;

void meterLadrillo()
{
	pthread_mutex_lock (&llaveAlmacen);
	almacen++;
	pthread_mutex_unlock (&llaveAlmacen);
}

int sacarLadrillo(int ladrillos, pthread_t thread_id)
{
	pthread_mutex_lock (&llaveAlmacen);
	if (almacen > 0)
	{
		if (almacen>=ladrillos)
		{
			almacen -= ladrillos;
			printf("%li: ha cogido %i ladrillos\n",thread_id, ladrillos);
		}
		else
		{
			printf("%li: no hay ladrillos suficientes\n", thread_id);
			ladrillos=0;
		}
	}
	else
	{
		printf("%li: no hay ningún ladrillo\n", thread_id);
		ladrillos = 0;
		if (estadoFabrica == false)
		{
			ladrillos=-1;
			printf("%li: la fábrica está cerrada\n", thread_id);
		}
	}
	pthread_mutex_unlock (&llaveAlmacen);
	return ladrillos;
}

void* fabrica (void*)
{
	estadoFabrica = true;
	for (int cnt=0; cnt<TLADRILLOS; cnt ++)
	{
		usleep(TIEMPOCREACION);
		meterLadrillo();
		printf("creo %i ladrillo\n", cnt);
	}
	estadoFabrica = false;
}



void* obrero(void*)
{
	pthread_t thread_id = pthread_self();
	int ladrillos,sacados;
	int totalsacados=0;
	while (true){
		ladrillos = 1+rand() %2;
		sacados=sacarLadrillo(ladrillos, thread_id);
		if ( sacados < 0 )
		{
			pthread_exit(NULL);
		}
		totalsacados += sacados;
		printf("%li: lleva sacados %d ladrillos\n", thread_id, totalsacados);
		usleep(ladrillos*10000);
	}
}

int main ()
{
	pthread_t thread1_id, thread2_id, thread3_id;

	printf("Pongo la fabrica a funcionar\n");
	pthread_create(&thread1_id,NULL, &fabrica, NULL);
	while(estadoFabrica == false)
	{
		usleep(10000);
	}
	printf("llamo a los obreros\n");
	pthread_create(&thread2_id,NULL, &obrero, NULL);
	pthread_create(&thread3_id, NULL, &obrero, NULL);

	pthread_join(thread1_id,NULL);
	pthread_join(thread2_id,NULL);
	pthread_join(thread3_id,NULL);

	return EXIT_SUCCESS;
}
