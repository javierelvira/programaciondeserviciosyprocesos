#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int spawn (char* program, char** arg_list)
{
      pid_t child_pid;
      child_pid = fork ();

      if(child_pid != 0){
          return child_pid;
      }
      else
      {
        execvp (program, arg_list);
        fprintf (stderr, "Ha ocurrido un error en execvp\n");
        abort ();
      }
}

int main ()
{
    pid_t pid_child;

    int child_status;
    char* arg_list[] = {(char *) "./son", NULL};

    pid_child = spawn ((char *) "./son", arg_list);

    int i = 0;

    while(i < 3){
        sleep(1);
        i++;
        kill(pid_child, SIGUSR1);

        printf("Enviadas: %i señales\n", i);
    }

    wait(&child_status);

    if(WIFEXITED (child_status) )
        printf ("El proceso hijo a salido bien\n");
    else
        printf("El proceso hijo no ha salido bien\n");

    return 0;
}
