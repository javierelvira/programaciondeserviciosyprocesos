#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define DELAY 2

sig_atomic_t sigusr1_count = 0;

/*Funcion que contabiliza el numero de señales que recibe*/
void handler (int signal_numbre)
{
    ++sigusr1_count;
}

int main()
{
    int n;
    struct sigaction sa;              //definimos la estrucutura sa que es de tipo signation
    memset (&sa, 0, sizeof(sa));      //inicializamos la estructura a 0
    sa.sa_handler = &handler;         //el sa_handler de sa sera igual a lo que hay en handler
    sigaction (SIGUSR1, &sa, NULL);   //indicamos que escuche las señales que reciba de SIGUSR1


    /*Saldra del while cuando haya recibido 3 señales de SIGUSR1*/
    while(sigusr1_count < 3){
        sleep(DELAY);                         //el programa duerme 2 segundos
        printf("Hola %i\n", sigusr1_count);   //mensaje de comprobacion
    }

    printf ("\nSIGUSR1 es repetido : %i veces \n", sigusr1_count);

    return 0;
}
