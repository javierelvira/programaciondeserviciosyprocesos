//Creado por Arturo Sierra
//Programa hijo -> son.cpp

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define DELAY 1               //timepo de espera, en segundos
#define SON_PROGRAM "./son"   //programa que ejecutara el hijo
#define N_SIGNAL 3            //numero de señales que queremos mandar al hijo

int spawn (char* program, char** arg_list)
{
    pid_t child_pid;                    //pid_t es igual a un int
    child_pid = fork ();                //El fork divide el programa en 2, siendo el padre y el hijo iguales exepto en el return

    /* Aqui comienzan los dos programas a ejecutarse*/

    //Comprobamos si el padre devuelve un valor
    if(child_pid !=0){
        return child_pid;
    }
    else
    {
        execvp (program, arg_list); // Se ejecuta el programa hijo, si execvp devuelve algo es que ha ocurrido un error
        //Si se ejecuta lo siguiente es que ha ocurrido un error y se cerrara el programa
        fprintf (stderr, "Un error ocurrio en execvp\n");
        abort ();
    }
}

int main ()
{
    pid_t pid_child; //Guardamos el pid del hijo

    int child_status;
    char* arg_list[] = {(char *) SON_PROGRAM, NULL};   //Lista de parametros

    pid_child = spawn ((char *) SON_PROGRAM, arg_list); //Llamamos a la funcion spawn y nos devuelve el valor del hijo

    int i = 0;

    //Enviamos 3 señales
    while(i < N_SIGNAL){
        sleep(DELAY);  //El programa se queda dormido 1 segundo
        i++;
        kill(pid_child, SIGUSR1);   //Enviamos la señal SIGUSR1 al programa hijo
        printf("Enviadas: %i señales\n", i);  //Imprime las señales que llevamos enviadas
    }

    wait(&child_status);  //Comprobamos si el estado del hijo a cambiado

    if(WIFEXITED (child_status) )
        printf ("El proceso hijo a salido bien\n");
    else
        printf ("El proceso hijo no ha salido bien\n");

    return 0;
}

