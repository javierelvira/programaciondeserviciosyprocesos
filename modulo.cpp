// un archivo que contenga dos librerias
// Hay que abrir ese archivo y que abra una de esas dos librerias
// las dos librerias tienen que tener funciones y una de esas funciones es la funcion catalogo, que muestra todas las funciones que hay


#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>

#include "aritmetica.h"

int main(){

    void *handle;

    handle = dlopen("./libtest.so", RTLD_LAZY);
    if (!handle){
        fprintf(stderr, "%s\n", dlerror());
        return EXIT_FAILURE;
    }
    dlerror();

    void (*test)() =(void (*)()) dlsym (handle, "suma");
    (*test)();

    dlclose (handle);

    return EXIT_SUCCESS;
}
