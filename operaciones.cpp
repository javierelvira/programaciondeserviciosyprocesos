/*Crear las funciones sumar y restar
 * Compilar el punto o
 * Generar el archivo libaritmetica.a
 * Crear un programa que incluya aritmetica.h
 * compilar el programa
 * linkar el programa con -laritmetica
 * ejecutarlo*/

#include <stdio.h>
#include <stdlib.h>
#include "aritmetica.h"

int sumar(int num1, int num2){

    return num1+num2;
}

int restar(int num1, int num2){

    return num1-num2;
}
