#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t mutex;
int cnt = 0;

void set_thread_flag (int flag_value)
{
	pthread_mutex_lock (&mutex);
	thread_flag = flag_value;
	pthread_cond_signal (&thread_flag_cv);
	pthread_mutex_unlock (&mutex);
}	

void do_work(){

	cnt++;
}

void* do_work2 (void* thread_arg)
{
	while (1) {
		if(cnt%5 == 0){
			printf("Segundos: %i\n", cnt);
			set_thread_flag(1);
		}
		usleep(1000000);
	}

	return NULL;
}

void initialize_flag ()
{
	pthread_mutex_init (&mutex, NULL);
	pthread_cond_init (&thread_flag_cv, NULL);
	thread_flag = 0;
}

void* thread_function (void* thread_arg)
{
	while (1)
	{
		pthread_mutex_lock (&mutex);
		while(!thread_flag)
			pthread_cond_wait (&thread_flag_cv, &mutex);
		pthread_mutex_unlock (&mutex);
		do_work();
		usleep(1000000);
	}
	return NULL;
}

int main () {

	pthread_t thread_id, thread2_id;
	initialize_flag();
	pthread_create(&thread_id, NULL, &thread_function, NULL);
	pthread_create(&thread2_id, NULL, &do_work2, NULL);

	pthread_join(thread_id, NULL);
	pthread_join(thread2_id, NULL);
}
