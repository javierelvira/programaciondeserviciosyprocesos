#ifndef __LIB1_H__
#define __LIB1_H__


#ifdef __cplusplus
extern "C" {
#endif

    const char **catalogo();
    int suma (int op1, int op2);
    int resta (int op1, int op2);

#ifdef __cplusplus
}
#endif



#endif

