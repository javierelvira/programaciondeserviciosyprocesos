#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <strings.h>

struct TPila {

    long dato[100];
    int cima=0;
};

int push (struct TPila *p, long id){
    p->dato[p->cima] = id;
    p->cima++;
    printf("Se ha metido: %li", id);
}

int pop (struct TPila *p) {
    int dato;
    p-> cima--;
    dato = p->dato[p->cima];

    return dato;
}

struct verid{
    int *id;
    int count;
};

enum { lleno, vacio, ERRORES };
int error = vacio;

struct TCola {
    int data[100];
    int base;
    int cima;
}

void push (struct TCola *cola, int dato) {
    error = 0;

    if (cola->cima - cola ->base >= 100) {
        error = lleno;
        return;
    }
    cola->data[cola->cima++ % 100] = dato;


int shift (struct TCola *cola) {
    error = 0;

    if (cola->cima - cola->base <= 0) {
        error = vacio;
        return error;
    }
}


void* show_thread_id(void* arg){

    FILE *pFile;
    long id = *((long*) arg);
    pFile = fopen("text.txt", "w");
    fprintf(pFile, "%li", id);
    fclose(pFile);

    return NULL;
}

long crearHilo(pthread_t thread_id){

    struct verid thread_args;
    pthread_create(&thread_id, NULL, &show_thread_id, &thread_id);
    printf("se ha creado el hilo %li\n", thread_id);
  return thread_id;
}

void cerrarHilo(pthread_t thread_id){
    pthread_join (thread_id, NULL);
    printf("Se ha cerrado el hilo %li\n", thread_id);
}

int main()
{
    struct TPila Pila;
    bzero (&Pila, sizeof(Pila));

    int opcion;
    pthread_t thread_id;

    do{
        printf("Opción 1: Crear un hilo\n");
        printf("Opción 2: Borrar el último hilo\n");
        printf("Opción 3: Cerrar hilos y salir\n");
        scanf("%i", &opcion);

        switch(opcion) {

            case 1:
                thread_id = crearHilo(thread_id);
                push(&Pila, thread_id);
                break;
            case 2:
                //borrarHilo(thread_id);
                printf("%i\n", Pila.cima);
                break;
            case 3:
                for (int i = 0; i < Pila.cima; i++){
                    cerrarHilo(Pila.dato[i]);
                }
                break;
            default:
                printf("Elija una opcion del 1 al 3\n");
        }

    }while (opcion!=3);

    return EXIT_SUCCESS;
}
