#ifndef __ARITMETICA_H__
#define __ARITMETICA_H__

#ifdef __cplusplus
extern "C"{
#endif

int sumar(int num1, int num2);
int restar(int num1, int num2);
#ifdef __cplusplus
}
#endif

#endif
