#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>


pid_t spawn (const char *program, char *lc [])

{

    pid_t child_pid = fork ();

    if (child_pid != 0)
    {

    }
    else {
        execvp (program, lc);
        fprintf (stderr, "No he podido arrancar el programa suma.\n");
        abort ();
    }
    return child_pid;
}

int main (int argc, char *argv []) {

    pid_t child_pid;
    int child_status;
    int resultado;

    child_pid = spawn ("./suma", argv);
    wait (&child_status);

        if (WIFEXITED (child_status)){
          resultado = WEXITSTATUS (child_status);
          printf ("La suma es %i\n", resultado);
        }

    return EXIT_SUCCESS;
}
